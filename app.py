from flask import (
    Flask, 
    jsonify, 
    abort, 
    request, 
    render_template, 
    redirect,
    url_for,
    flash
)
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.secret_key = os.urandom(24)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///books.db'
db = SQLAlchemy(app)

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    author = db.Column(db.String(50))
    published_year = db.Column(db.Integer)


@app.route('/books', methods=['GET'])
def get_books():
    # return a list of books
    books = Book.query.all()
    return render_template('books.html', books=books)


@app.route('/books/<int:book_id>', methods=['GET'])
def get_book(book_id):
    # return a specific book
    book = Book.query.get(book_id)
    if book is None:
        abort(404)
    return render_template('book.html', book=book)


@app.errorhandler(404)
def not_found(error):
    return jsonify({'error': 'Not found'}), 404


@app.route('/books/add', methods=['GET', 'POST'])
def add_book():
    # add a new book
    if request.method == "GET":
        return render_template('add_book.html')
    elif request.method == "POST":
        if 'title' not in request.form:
            flash('Title is required')
            return redirect(request.url)
        book = Book(title=request.form['title'], author=request.form.get('author', ''), published_year=request.form.get('published_year', ''))
        db.session.add(book)
        db.session.commit()
        flash('Book added successfully')
        return redirect(url_for('get_books'))



@app.route('/books/<int:book_id>', methods=['GET', 'PUT'])
def update_book(book_id):
    # update an existing book
    book = Book.query.get(book_id)
    if book is None:
        abort(404)
    if request.method == "PUT":
        if book is None:
            abort(404)
        if not request.json:
            abort(400)
        if 'title' in request.json:
            book.title = request.json['title']
        if 'author' in request.json:
            book.author = request.json['author']
        if 'published_year' in request.json:
            book.published_year = request.json['published_year']
        db.session.commit()
        return jsonify({'book': {'id': book.id, 'title': book.title, 'author': book.author, 'published_year': book.published_year}})
    return render_template('update_book.html', book=book)


@app.route('/books/<int:book_id>/delete', methods=['GET', 'POST', 'DELETE'])
def delete_book_confirmation(book_id):
    # display confirmation template for book delete action
    book = Book.query.get(book_id)
    if book is None:
        abort(404)
    if request.method == 'GET':
        return render_template('delete_book_confirmation.html', book=book)
    elif request.method == 'POST':
        db.session.delete(book)
        db.session.commit()
        return redirect(url_for('get_books'))
